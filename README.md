[![pipeline status](https://gitlab.windenergy.dtu.dk/OpenLAC/BasicDTUControllerReport/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/OpenLAC/BasicDTUControllerReport/commits/master)

Welcome to the Basic DTU Controller Report
------------------

Check out the newest development version here:
[Basic DTU Controller Report](https://openlac.pages.windenergy.dtu.dk/BasicDTUControllerReport).
